import { glClientSDK } from '@gitlab/application-sdk-browser'

window.glClient = null

const createClient = () => {
  if (window.glClient) {
    return
  }

  window.glClient = glClientSDK({
    appId: process.env.GITLAB_ANALYTICS_ID,
    host: process.env.GITLAB_ANALYTICS_URL,
  })
}

export default ({ app }) => {
  createClient()
  app.router.afterEach(() => {
    window.glClient?.page()
  })
}
