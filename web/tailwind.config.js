module.exports = {
  important: true,
  safelist: [
    'w-4',
    'h-4',
    'inline-flex',
    'items-center',
    'justify-center',
    'rounded',
    'no-underline',
    'bg-indigo-100',
    'hover:bg-indigo-300/50',
  ],
}
