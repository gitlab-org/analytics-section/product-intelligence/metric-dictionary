const APP_NAME = 'GitLab Metrics Dictionary'
const APP_DESC =
  'Browse through definitions for Service Ping metrics and Snowplow events'

const GITLAB_ANALYTICS_ID = process.env.GITLAB_ANALYTICS_ID ?? false
const GITLAB_ANALYTICS_URL = GITLAB_ANALYTICS_ID
  ? 'https://collector.prod-1.gl-product-analytics.com'
  : ''

if (GITLAB_ANALYTICS_ID) {
  // eslint-disable-next-line no-console
  console.log(
    `GITLAB_ANALYTICS_ID found and applied. Sending events to ${GITLAB_ANALYTICS_URL}`,
  )
} else {
  console.log(`GITLAB_ANALYTICS_ID disabled`) // eslint-disable-line no-console
}

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',
  ssr: false,
  telemetry: false,

  generate: {
    dir: '../public',
    routes: ['/keys/service-ping', '/keys/snowplow'],
  },
  /*
   ** Pass environment variables to webpack's DefinePlugin
   */
  env: {
    GITLAB_ANALYTICS_URL,
    GITLAB_ANALYTICS_ID,
  },

  server: {
    port: 9080,
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: APP_NAME,
    titleTemplate: `%s • ${APP_NAME}`,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: APP_DESC },
      { hid: 'og:title', name: 'og:title', content: APP_NAME },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://rsms.me/inter/inter.css' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com' },
      {
        href: 'https://fonts.googleapis.com/css2?family=Source+Code+Pro:wght@500&display=swap',
        rel: 'stylesheet',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['@gitlab/ui/dist/index.css', '@gitlab/ui/dist/utility_classes.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/gitlab-ui.js',
    '@/plugins/gridjs.js',
    GITLAB_ANALYTICS_ID
      ? { src: '~/plugins/analytics.js', mode: 'client' }
      : false,
  ].filter(Boolean),

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: '/',
  },

  router: {
    extendRoutes(routes, resolve) {
      routes.push({
        path: '/snowplow',
        redirect: '/events',
      })
    },
  },

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    meta: {
      name: APP_NAME,
      author: APP_NAME,
      description: APP_DESC,
      theme_color: '#2F2A6B',
    },
    manifest: {
      lang: 'en',
      name: APP_NAME,
      description: APP_DESC,
      background_color: '#FFFFFF',
    },
    workbox: {
      assetsURLPattern: ['/_nuxt/', '/data/'],
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
