import { getSearchQuery, setSearchQuery } from '@/assets/get-search-query'
import interpretVariables from '@/assets/interpret-variables'
import { fetchSearchResults } from '@/assets/utils'
import { DASHBOARD } from '~/assets/constants'

export default {
  mounted() {
    const columnsVisibilitySeries =
      this.localStorage('columns-visibility') || []

    if (columnsVisibilitySeries.length) {
      this.columns = this.columns.map((column, index) => ({
        ...column,
        hidden: columnsVisibilitySeries[index],
      }))
    }

    this.paginationLimit = this.localStorage('pagination-limit') || 15
  },

  methods: {
    getCellButtonClasses({ hover = true } = {}) {
      const classes = [
        'w-4',
        'h-4',
        'inline-flex',
        'items-center',
        'justify-center',
        'rounded',
        'no-underline',
        'bg-indigo-100',
      ]

      if (hover) {
        classes.push('hover:bg-indigo-300/50')
      }

      return classes.join(' ')
    },

    handleSearch(term) {
      const table = this.$refs.table.$el

      const $searchBox = table.querySelector('.gridjs-search-input')

      if ($searchBox) {
        const inputEvent = new Event('input', {
          bubbles: true,
          cancelable: true,
        })

        $searchBox.value = term
        $searchBox.dispatchEvent(inputEvent)
      }
    },

    handleColumnsChange(columns) {
      this.columns = columns
    },

    handlePaginationLimitChange(value) {
      this.paginationLimit = value
    },

    interpretVariables(cell) {
      const { html } = this.$gridjs
      const variablesRegex = /(?:\s\||^`|`$)/g

      if (!cell || !variablesRegex.test(cell)) {
        return cell
      }

      return html(interpretVariables(cell))
    },

    buildSharePath(rows) {
      const searchQuery = getSearchQuery()

      if (!searchQuery || !rows.length) {
        this.sharePath = ''

        return
      }

      let sharePath = ''
      const STRING_LIMIT = 2000

      for (const row of rows) {
        const key = row.key_path || row.key || ''

        if (!key) {
          continue
        }

        if (sharePath.concat(key).length > STRING_LIMIT) {
          break
        }

        sharePath = `${sharePath}${sharePath ? ',' : ''}${key}`
      }

      if (!sharePath) {
        this.sharePath = ''

        return
      }

      this.sharePath = `/keys/${this.namespace}?q=${sharePath.trim(',')}`

      this.$nextTick(() => {
        window.glClient?.refreshLinkClickTracking()
      })

      // track table searches
      if (searchQuery) {
        window.glClient?.track('enter_search_term', {
          searchTerm: decodeURIComponent(window.location.search?.slice(1)),
        })
      }
    },

    async fetchData() {
      const response = await fetch(this.dataUrl)
      const data = await response.json()
      this.fetchedData = data
      this.rows = this.fetchedData
    },
    submitSearch(tokens, context) {
      if (this.context === DASHBOARD) {
        this.validateFilters(tokens)
      }

      this.rows = fetchSearchResults(tokens, this.fetchedData, context)
      this.buildSharePath(this.rows)
    },
    clearSearch() {
      setSearchQuery({})
      this.rows = this.fetchedData
    },
  },
}
