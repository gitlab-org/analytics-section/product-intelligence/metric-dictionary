/**
 * Gets current data manifest while busting the cache.
 *
 * @async
 * @returns {Promise<Object>}
 */
export default async function getDataManifest() {
  const headers = new Headers()
  headers.append('pragma', 'no-cache')
  headers.append('cache-control', 'no-cache')

  const request = new Request('/data/manifest.json')

  const response = await fetch(request, {
    method: 'GET',
    headers,
  })

  return response.json()
}
