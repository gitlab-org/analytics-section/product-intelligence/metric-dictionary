# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/metric_row"
require "json"

RSpec.describe MetricRow do
  subject(:row) { described_class.new(data) }
  let(:tiers) { ["free", "premium", "ultimate"] }
  let(:distribution) { ["ee", "ce"] }
  let(:instrumentation_class) { "CountUsersCreatingCiBuildsMetric" }
  let(:data_source) { "database" }
  let(:events) { nil }

  let(:data) do
     {
       "id"=>"gid://gitlab/Blob/edf11470cb301a7e614b8bd8605fd289b8049233",
        "name"=>"20210201124934_deployments.yml",
        "webPath"=>"/gitlab-org/gitlab/-/blob/master/config/metrics/counts_all/20210201124934_deployments.yml",
        "rawBlob"=> {
          "data_category"=>"optional",
          "key_path"=>"counts.deployments",
          "description"=>"Total deployments count",
          "product_group"=>"release",
          "product_categories"=>["deployment_management"],
          "value_type"=>"number",
          "status"=>"active",
          "milestone"=>"8.12",
          "introduced_by_url"=>"https://gitlab.com/gitlab-org/gitlab/-/merge_requests/735",
          "time_frame"=>"all",
          "data_source"=> data_source,
          "instrumentation_class" => instrumentation_class,
          "distribution"=> distribution,
          "tiers"=> tiers,
          "performance_indicator_type"=>["smau", "gmau", "paid_gmau"],
          "options"=>["events" => ["event_a", "event_b"]],
          "events"=> events
        }
      }
  end

  it "returns performance_indicator_type" do
    expect(row.performance_indicator_type).to eq("smau, gmau, paid_gmau")
  end

  it "returns distribution" do
    expect(row.distribution).to eq("ee, ce")
  end

  it "returns a valid gitlab-com Service Ping Snowflake query" do
    expected_query = <<-SQL
      SELECT
        ping_created_at,
        metrics_path,
        metric_value,
        has_timed_out --if metric timed out, the value will be set to 0
      FROM common.fct_ping_instance_metric_rolling_6_months --model limited to last 6 months for performance
      WHERE dim_instance_id = 'ea8bf810-1d6f-4a6a-b4fd-93e8cbd8b57f' --SaaS instance, both production and staging installations
      AND metrics_path = 'counts.deployments' --set to metric of interest
      ORDER BY ping_created_at DESC
      LIMIT 5
      ;
    SQL

    expect(row.gitlab_com_snowflake_query).to eq(expected_query)
  end

  it "returns a valid self-managed Service Ping Snowflake query" do
    expected_query = <<-SQL
      SELECT
        ping_created_at,
        metrics_path,
        metric_value,
        has_timed_out --if metric timed out, the value will be set to 0
      FROM common.fct_ping_instance_metric_rolling_6_months --model limited to last 6 months for performance
      WHERE dim_instance_id = <INSTANCE_ID> -- self-managed instance id
      AND metrics_path = 'counts.deployments' --set to metric of interest
      ORDER BY ping_created_at DESC
      LIMIT 5
      ;
    SQL

    expect(row.self_managed_service_ping_snowflake_query).to eq(expected_query)
  end

  context 'with internal events metric' do
    let(:data_source) { "internal_events" }
    let(:events) do
      [
        { "name" => "event_a", "unique" => "user.id" },
        { "name" => "event_b", "unique" => "project.id" }
      ]
    end

    it "returns a valid gitlab-com Snowplow Snowflake query" do
      expected_query = <<-SQL
      SELECT
        ultimate_parent_namespace_id, --or you can use dim_namespace_id if you want the more granular value
        event_action,
        event_category,
        event_label,
        event_property,
        COUNT(*) AS event_count
      FROM common_mart.mart_behavior_structured_event
      WHERE behavior_at >= CURRENT_DATE-30 --last 30 days
        AND dim_namespace_id IS NOT NULL --assuming you want this filter
        AND event_action = 'event_a'
      GROUP BY 1,2,3,4,5
      ORDER BY event_count DESC
      LIMIT 1000
      ;
    SQL

      expect(row.gitlab_com_snowflake_query).to eq(expected_query)
    end
  end

  context "returns all tiers" do
    subject { row.tiers }

    context "with all tiers" do
      let(:tiers) { ["free", "premium", "ultimate"] }

      it { is_expected.to eq(["free", "premium", "ultimate"]) }
    end

    context "with premium and ultimate" do
      let(:tiers) { ["premium", "ultimate"] }

      it { is_expected.to eq(["premium", "ultimate"]) }
    end

    context "with ultimate" do
      let(:tiers) { ["ultimate"] }

      it { is_expected.to eq(["ultimate"]) }
    end

    context "with no tiers" do
      let(:tiers) { [] }

      it { is_expected.to eq([]) }
    end
  end

  context "instrumentation class" do
    subject { row.instrumentation_class_link }

    context "in CE and EE" do
      let(:distribution) { ["ce", "ee"] }

      it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage/metrics/instrumentations/count_users_creating_ci_builds_metric.rb") }
    end

    context "only in EE" do
      let(:distribution) { ["ee"] }

      it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/ee/lib/gitlab/usage/metrics/instrumentations/count_users_creating_ci_builds_metric.rb") }
    end

    context "instrumentation class within a module" do
      let(:instrumentation_class) { "ModuleName::CountUsersCreatingCiBuildsMetric" }

      it { is_expected.to eq("https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/usage/metrics/instrumentations/module_name/count_users_creating_ci_builds_metric.rb") }
    end
  end
end
