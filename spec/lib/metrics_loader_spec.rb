# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/metrics_loader"

RSpec.describe MetricsLoader do
  let(:instance) { described_class.new }
  let(:metrics_api_response) do
    [
      {
        'key_path' => 'count.metric_name_1',
        'file_path' => 'metrics/20210101_metric_name_1.yml'
      }, {
        'key_path' => 'count.metric_name_2',
        'file_path' => 'metrics/20210101_metric_name_2.yml'
      }
    ].to_yaml
  end

  before do
    metrics_api = double(MetricDefinitionsApi)
    allow(MetricDefinitionsApi).to receive(:new).and_return(metrics_api)
    allow(metrics_api).to receive(:fetch).and_return(metrics_api_response)
  end

  after do
    FileUtils.rm_f('service_ping_data.json')
  end

  describe "#fetch" do
    it "uses MetricDefinitionsApi" do
      metrics_api = double(MetricDefinitionsApi)
      expect(MetricDefinitionsApi).to receive(:new).and_return(metrics_api)
      expect(metrics_api).to receive(:fetch).and_return(metrics_api_response)

      instance.fetch
    end

    it "loads proper data" do
      instance.fetch
      instance.save

      expect(instance.load).to match_array(
        [
          { "webPath" => "/gitlab-org/gitlab/-/blob/master/metrics/20210101_metric_name_1.yml", "rawBlob" => "---\nkey_path: count.metric_name_1\n" },
          { "webPath" => "/gitlab-org/gitlab/-/blob/master/metrics/20210101_metric_name_2.yml", "rawBlob" => "---\nkey_path: count.metric_name_2\n" },
        ]
      )
    end
  end
end
