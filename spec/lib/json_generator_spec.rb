# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/json_generator"

RSpec.describe JsonGenerator do
  subject(:instance) { described_class.new }

  describe "#generate" do
    let(:service_ping_data) do
      [
        {
          "rawBlob" => "tiers: [free]\nkey_path: metric_name",
          "name" => "20210101_metric_name"
        }
      ]
    end

    let(:events_data) do
      [
        {
          "rawBlob" => "name: metric_name2"
        }
      ]
    end

    it "only loads data once" do
      service_ping_loader = double(fetched?: false, save: nil)
      events_loader = double(fetched?: false, save: nil)

      expect(MetricsLoader).to receive(:new).and_return(service_ping_loader)
      expect(EventsLoader).to receive(:new).and_return(events_loader)

      expect(service_ping_loader).to receive(:fetch).once.and_return(service_ping_data)
      expect(events_loader).to receive(:fetch).once.and_return(events_data)

      instance.generate
    end
  end
end
