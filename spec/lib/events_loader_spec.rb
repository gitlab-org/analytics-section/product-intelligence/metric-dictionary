# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/events_loader"

RSpec.describe EventsLoader do
  let(:instance) { described_class.new }
  let(:tree_api_response) do
    [
      {
        "path" => "file_path_1.yml"
      }, {
        "path" => "file_path_2.yml"
      }
    ]
  end
  let(:blob_api_response) do
    [
      {
        "rawBlob" => "raw_blob_1",
        "name" => "file_path_1.yml"
      }, {
        "rawBlob" => "---\nintroduced_by_url: www.gitlab.com",
        "name" => "file_path_3.yml"
      }
    ]
  end

  before do
    EVENT_SOURCES.each do
      tree_api = double(TreeApi)
      allow(TreeApi).to receive(:new).and_return(tree_api)
      allow(tree_api).to receive(:fetch).and_return(tree_api_response)

      blob_api = double(BlobApi)
      allow(BlobApi).to receive(:new).and_return(blob_api)
      allow(blob_api).to receive(:fetch) { blob_api_response.map &:dup }
    end
  end

  after do
    FileUtils.rm_f('event_data.json')
  end

  describe "#fetch" do
    let(:instance) { described_class.new }

    it "uses TreeApi with proper arguments" do
      instance.fetch

      EVENT_SOURCES.each do |event_source|
        event_source[:directories].each do |folder_name|
          expect(TreeApi).to have_received(:new).with(event_source[:project_path], folder_name)
        end
      end
    end

    it "uses BlobApi with proper arguments" do
      instance.fetch

      expect(BlobApi).to have_received(:new).with(DEFAULT_PROJECT_PATH, %w(file_path_1.yml file_path_2.yml)).twice
    end

    it "loads proper data" do
      instance.fetch
      instance.save

      expect(instance.load).to match_array(
        [
          { "name" => "file_path_1.yml", "rawBlob" => "raw_blob_1", "service_name" => "GitLab" },
          { "name" => "file_path_3.yml", "rawBlob" => "---\nintroduced_by_url: www.gitlab.com", "service_name" => "GitLab" },
          { "name" => "file_path_1.yml", "rawBlob" => "raw_blob_1", "service_name" => "GitLab" },
          { "name" => "file_path_3.yml", "rawBlob" => "---\nintroduced_by_url: www.gitlab.com", "service_name" => "GitLab" },
          { "name" => "file_path_1.yml", "rawBlob" => "raw_blob_1", "service_name" => "AI Gateway" },
          { "name" => "file_path_3.yml", "rawBlob" => "---\nintroduced_by_url: www.gitlab.com", "service_name" => "AI Gateway" },
          { "name" => "file_path_1.yml", "rawBlob" => "raw_blob_1", "service_name" => "Duo Workflow" },
          { "name" => "file_path_3.yml", "rawBlob" => "---\nintroduced_by_url: www.gitlab.com", "service_name" => "Duo Workflow" },
          { "name" => "file_path_1.yml", "rawBlob" => "raw_blob_1", "service_name" => "Switchboard" },
          { "name" => "file_path_3.yml", "rawBlob" => "---\nintroduced_by_url: www.gitlab.com", "service_name" => "Switchboard" }
        ]
      )
    end

    context "files need to be split into separate batches" do
      let(:tree_api_response) do
        (EventsLoader::FILE_BATCH_SIZE + 1).times.map do |index|
          {
            "path" => "file_path_#{index}.yml"
          }
        end
      end

      before do
        stub_const("#{described_class}::FILE_BATCH_SIZE", 15)
      end

      it "loads the files in batches" do
        instance.fetch

        file_paths = tree_api_response.map { |file| file["path"] }
        expect(BlobApi)
          .to have_received(:new).with(DEFAULT_PROJECT_PATH, file_paths[0..(EventsLoader::FILE_BATCH_SIZE - 1)]).twice
        expect(BlobApi)
          .to have_received(:new).with(DEFAULT_PROJECT_PATH, file_paths[EventsLoader::FILE_BATCH_SIZE..-1]).twice
      end
    end
  end
end
