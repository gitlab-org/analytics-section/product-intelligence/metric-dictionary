# frozen_string_literal: true

require "./spec/spec_helper"
require "./lib/builder"

RSpec.describe Builder do
  let(:instance) { described_class.new }

  let(:events_data) do
    [
      {
        "rawBlob" => "tiers: [free]\naction: event_name_1\nintroduced_by_url: mr_web_url_1",
        "name" => "20210101_event_name_1.yml",
        "service_name" => "Service 1"
      }, {
        "rawBlob" => "tiers: [free]\naction: event_name_2\nintroduced_by_url: mr_web_url_2",
        "name" => "20210104_event_name_2.yml",
        "service_name" => "Service 2"
      }
    ]
  end

  let(:metrics_data) do
    [
      {
        "rawBlob" => "key_path: count.metric_name_1\ntiers:\n- free\nintroduced_by_url: mr_web_url_1",
        "webPath" => "/gitlab-org/gitlab/-/blob/master/metrics/20210101_metric_name_1.yml"
      }, {
        "rawBlob" => "key_path: count.metric_name_2\ntiers:\n- free\nintroduced_by_url: mr_web_url_2",
        "webPath" => "/gitlab-org/gitlab/-/blob/master/metrics/20210101_metric_name_2.yml"
      }
    ]
  end

  before do
    metrics_loader = instance_double(MetricsLoader, load: metrics_data, fetched?: true)
    allow(MetricsLoader).to receive(:new).and_return(metrics_loader)
    events_loader = instance_double(EventsLoader, load: events_data, fetched?: true)
    allow(EventsLoader).to receive(:new).and_return(events_loader)
  end

  after do
    FileUtils.rm_f('service_ping_data.json')
    FileUtils.rm_f('event_data.json')
  end

  it "builds csv data correctly" do
    instance.build

    expected_event_data = [
      ["action", "introduced_by_url", "tiers"],
      ["event_name_1", "mr_web_url_1", "free"],
      ["event_name_2", "mr_web_url_2", "free"]
    ]
    expected_metrics_data = [
      ["introduced_by_url", "key_path", "tiers"],
      ["mr_web_url_1", "count.metric_name_1", "free"],
      ["mr_web_url_2", "count.metric_name_2", "free"],
    ]
    expect(CSV.read("web/static/data/service_ping.csv")).to match_array(expected_metrics_data)
    expect(CSV.read("web/static/data/snowplow.csv")).to match_array(expected_event_data)
  end

  it "builds json data correctly" do
    instance.build

    manifest = JSON.parse(File.read("web/static/data/manifest.json"))

    service_ping_data_template = {
      "key_path" => "count.metric_name_1",
      "introduced_by_url" => "mr_web_url_1",
      "tiers" => ["free"],
      "web_path" => '/gitlab-org/gitlab/-/blob/master/metrics/20210101_metric_name_1.yml'
    }
    service_ping_data_template2 = service_ping_data_template.merge(
      "key_path" => "count.metric_name_2",
      "introduced_by_url" => "mr_web_url_2",
      "web_path" => '/gitlab-org/gitlab/-/blob/master/metrics/20210101_metric_name_2.yml'
    )
    expected_service_ping_data = [
      a_hash_including(service_ping_data_template),
      a_hash_including(service_ping_data_template2)
    ]
    service_ping_json = JSON.parse(File.read("web/static/data/service_ping.#{manifest['service_ping']}.json"))
    expect(service_ping_json).to match(expected_service_ping_data)

    snowplow_data_template = { "key" => "20210101_event_name_1", "tiers" => ["free"] }
    snowplow_data_template2 = snowplow_data_template.merge("key" => "20210104_event_name_2")
    expected_snowplow_data = [
      a_hash_including(snowplow_data_template),
      a_hash_including(snowplow_data_template2)
    ]
    snowplow_json = JSON.parse(File.read("web/static/data/snowplow.#{manifest['snowplow']}.json"))
    expect(snowplow_json).to match(expected_snowplow_data)
  end
end
