require "date"
require "uri"

class MetricRow
  SERIALIZED_ATTRIBUTES = [:time_frame, :data_source, :data_category, :status, :product_group,
    :milestone, :instrumentation_class, :key_path, :web_path, :tiers, :description,
    :introduced_by_url, :value_type, :options, :events, :product_categories]
  SERIALIZED_METHODS = [:performance_indicator_type, :tiers, :distribution, :gitlab_com_snowflake_query,
                        :self_managed_service_ping_snowflake_query, :instrumentation_class_link]

  GITLAB_BASE_URL = "https://gitlab.com/gitlab-org/gitlab/-/blob/master/"
  INSTRUMENTATION_PATH = "lib/gitlab/usage/metrics/instrumentations/"
  INTRODUCED_BY_URL_KEY = "introduced_by_url"

  attr_reader *SERIALIZED_ATTRIBUTES

  def initialize(data)
    @data = data

    @time_frame = data["rawBlob"]["time_frame"]
    @data_source = data["rawBlob"]["data_source"]
    @data_category = data["rawBlob"]["data_category"]
    @status = data["rawBlob"]["status"]
    @product_group = data["rawBlob"]["product_group"]
    @milestone = data["rawBlob"]["milestone"]
    @key_path = data["rawBlob"]["key_path"]
    @description = data["rawBlob"]["description"]
    @instrumentation_class = data["rawBlob"]["instrumentation_class"]
    @introduced_by_url = data["rawBlob"][INTRODUCED_BY_URL_KEY]
    @value_type = data["rawBlob"]["value_type"]
    @web_path = data["webPath"]
    @options = data["rawBlob"]["options"]
    @events = data["rawBlob"]["events"]
    @product_categories = data["rawBlob"]["product_categories"]
  end

  def to_json(*args)
    SERIALIZED_ATTRIBUTES.dup.concat(SERIALIZED_METHODS).map { |method| [method.to_s, public_send(method)] }.to_h.to_json(*args)
  end

  def performance_indicator_type
    @data['rawBlob'].fetch('performance_indicator_type', []).join(', ')
  end

  def distribution
    @data['rawBlob'].fetch('distribution', []).join(', ')
  end

  def tiers
    @data['rawBlob'].fetch('tiers', []).sort
  end

  def gitlab_com_snowflake_query
    return gitlab_com_snowplow_snowflake_query if data_source == 'internal_events'

    gitlab_com_service_ping_snowflake_query
  end

  def gitlab_com_service_ping_snowflake_query
    service_ping_metric_query("'ea8bf810-1d6f-4a6a-b4fd-93e8cbd8b57f' --SaaS instance, both production and staging installations")
  end

  def self_managed_service_ping_snowflake_query
    service_ping_metric_query('<INSTANCE_ID> -- self-managed instance id')
  end

  def service_ping_metric_query(instance_id_string)
    <<-SQL
      SELECT
        ping_created_at,
        metrics_path,
        metric_value,
        has_timed_out --if metric timed out, the value will be set to 0
      FROM common.fct_ping_instance_metric_rolling_6_months --model limited to last 6 months for performance
      WHERE dim_instance_id = #{instance_id_string}
      AND metrics_path = \'#{key_path}\' --set to metric of interest
      ORDER BY ping_created_at DESC
      LIMIT 5
      ;
    SQL
  end

  def gitlab_com_snowplow_snowflake_query
    return unless data_source == 'internal_events'

    <<-SQL
      SELECT
        ultimate_parent_namespace_id, --or you can use dim_namespace_id if you want the more granular value
        event_action,
        event_category,
        event_label,
        event_property,
        COUNT(*) AS event_count
      FROM common_mart.mart_behavior_structured_event
      WHERE behavior_at >= CURRENT_DATE-30 --last 30 days
        AND dim_namespace_id IS NOT NULL --assuming you want this filter
        AND event_action = \'#{events.first['name']}\'
      GROUP BY 1,2,3,4,5
      ORDER BY event_count DESC
      LIMIT 1000
      ;
    SQL
  end

  def instrumentation_class_link
    return nil if instrumentation_class.nil?
    URI.join(GITLAB_BASE_URL, ee_or_ce_path, INSTRUMENTATION_PATH, instrumentation_class_file).to_s
  end

  private

  def ee_or_ce_path
    @data['rawBlob'].fetch('distribution', []).include?("ce") ? "" : "ee/"
  end

  def instrumentation_class_file
    class_name = @instrumentation_class
    return '' if class_name.nil? || class_name.empty?

    file_name = class_name.split("::").map do |part|
      part.gsub(/([A-Z]+)([A-Z][a-z])/, '\1_\2')
      .gsub(/([a-z\d])([A-Z])/, '\1_\2')
      .downcase
    end.join("/")

    file_name.concat(".rb")
  end
end
