# frozen_string_literal: true
#
require 'json'
require_relative 'api/metric_definitions_api'
require_relative 'constants'
require_relative 'metric_row'
require_relative 'parser'

class MetricsLoader
  FILENAME = 'service_ping_data.json'
  MASTER_PATH = '/-/blob/master/'

  def initialize
    @files = []
  end

  def fetch
    @files = fetch_metrics
  end

  def save
    File.write(FILENAME, JSON.generate(@files))
  end

  def fetched?
    File.exist?(FILENAME)
  end

  def refetch
    fetch
    save
  end

  def load
    JSON.parse(File.read(FILENAME))
  end

  private

  def fetch_metrics
    metrics_payload = MetricDefinitionsApi.new.fetch
    # map into BlobApi's response format
    webpath_prefix = "/#{DEFAULT_PROJECT_PATH}#{MASTER_PATH}"
    YAML.safe_load(metrics_payload).map do |metric_definition|
      {
        "webPath" => webpath_prefix + metric_definition.delete("file_path"),
        "rawBlob" => metric_definition.to_yaml
      }
    end
  end
end
