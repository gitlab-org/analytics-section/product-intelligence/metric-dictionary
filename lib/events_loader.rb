# frozen_string_literal: true

require 'json'
require_relative 'api/blob_api'
require_relative 'api/tree_api'
require_relative 'constants'
require_relative 'event_row'
require_relative 'parser'

class EventsLoader
  FILENAME = 'event_data.json'
  FILE_BATCH_SIZE = 20

  def initialize
    @files = []
  end

  def fetch
    @files = fetch_event_files
  end

  def save
    File.write(FILENAME, JSON.generate(@files))
  end

  def fetched?
    File.exist?(FILENAME)
  end

  def refetch
    fetch
    save
  end

  def load
    JSON.parse(File.read(FILENAME))
  end

  private

  def fetch_event_files
    EVENT_SOURCES.each do |source|
      source[:directories].each do |directory|
        load_files(source, directory)
      end
    end

    @files.flatten
  end

  def load_files(source, directory)
    project_path = source[:project_path]
    # Slicing to prevent graphql-query complexity failures
    TreeApi.new(project_path, directory).fetch.map { |f| f["path"] }.each_slice(FILE_BATCH_SIZE) do |file_paths|
      pp "Loading next batch #{file_paths}"
      raw_files = BlobApi.new(project_path, file_paths).fetch
      raw_files.each do |raw_file|
        raw_file['service_name'] = source[:name]
      end
      @files.push(raw_files)
    end
  end
end
