# frozen_string_literal: true

require 'uri'

class EventRow
  SERIALIZED_ATTRIBUTES = %i[description action label_description property_description
                             value_description extra_properties identifiers introduced_by_url
                             product_group milestone product_categories service_name].freeze
  SERIALIZED_METHODS = %i[key web_path tiers category].freeze

  GITLAB_BASE_URL = 'https://gitlab.com/gitlab-org/gitlab/-/blob/master/'

  attr_reader(*SERIALIZED_ATTRIBUTES)

  def initialize(data)
    @data = data

    @description = data['rawBlob']['description']
    @action = data['rawBlob']['action']
    @category = data['rawBlob']['category']
    @internal_events = data['rawBlob']['internal_events']
    @label_description = data.dig('rawBlob', 'additional_properties', 'label', 'description') || ''
    @property_description = data.dig('rawBlob', 'additional_properties', 'property', 'description') || ''
    @value_description = data.dig('rawBlob', 'additional_properties', 'value', 'description') || ''
    @extra_properties = data['rawBlob']['extra_properties']
    @identifiers = data['rawBlob']['identifiers']
    @introduced_by_url = data['rawBlob']['introduced_by_url']
    @product_group = data['rawBlob']['product_group']
    @milestone = data['rawBlob']['milestone']
    @product_categories = data["rawBlob"]["product_categories"]
    @service_name = data['service_name']
  end

  def to_json(*args)
    SERIALIZED_ATTRIBUTES.dup.concat(SERIALIZED_METHODS).map { |method| [method.to_s, public_send(method)] }.to_h.to_json(*args)
  end

  def key
    File.basename(@data['name'].to_s, '.yml')
  end

  def category
    @internal_events ? 'InternalEventsTracking' : @category
  end

  def tiers
    @data['rawBlob'].fetch('tiers', []).sort
  end

  def web_path
    return nil if @data['webPath'].nil?

    URI.join(GITLAB_BASE_URL, @data['webPath']).to_s
  end
end
