require 'net/http'

class MetricDefinitionsApi
  URL = URI('https://gitlab.com/api/v4/usage_data/metric_definitions?include_paths=true')

  def fetch
    Net::HTTP.get(URL)
  end
end
