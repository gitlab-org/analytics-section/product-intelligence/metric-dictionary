require_relative 'api'

class BlobApi < Api
  def initialize(project_path, file_paths)
    @project_path = project_path
    @file_paths = file_paths
  end

  private

  def parse(json)
    json.dig('data', 'project', 'repository', 'blobs', 'edges').map { |obj| obj["node"] }
  end

  def page_info_path
    ['project', 'repository', 'blobs']
  end

  def params
    @file_paths.map { |p| "\"#{p}\""}.join(',')
  end

  def project_path
    @project_path
  end

  def query
    <<-gql
    {
      project(fullPath: "%{project_path}") {
        id
        repository {
          blobs(after: "%{after}", paths: [%{params}]) {
            pageInfo {
              hasNextPage
              endCursor
            }
            edges {
              node{
                id
                name
                webPath
                rawBlob
              }
            }
          }
        }
      }
    }
    gql
  end
end
