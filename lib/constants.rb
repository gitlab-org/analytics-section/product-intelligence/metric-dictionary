# frozen_string_literal: true

DEFAULT_PROJECT_PATH = 'gitlab-org/gitlab'

EVENT_SOURCES = [
  {
    project_path: DEFAULT_PROJECT_PATH,
    name: "GitLab",
    directories: %w[config/events ee/config/events]
  },
  {
    project_path: 'gitlab-org/modelops/applied-ml/code-suggestions/ai-assist',
    name: "AI Gateway",
    directories: %w[config/events]
  },
  {
    project_path: 'gitlab-org/duo-workflow/duo-workflow-service',
    name: "Duo Workflow",
    directories: %w[duo_workflow_service/config/events]
  },
  {
    project_path: 'gitlab-com/gl-infra/gitlab-dedicated/switchboard-events',
    name: "Switchboard",
    directories: %w[config/events]
  }
]
