require_relative "csv_generator.rb"
require_relative "json_generator.rb"

class Builder
  def build
    JsonGenerator.new.generate
    CsvGenerator.new.generate
  end
end


