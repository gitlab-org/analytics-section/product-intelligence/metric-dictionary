require 'json'
require 'yaml'

class Parser
  def initialize(data)
    @data = data.map { |d| d.merge("rawBlob" => YAML.load(d["rawBlob"])) }
  end

  def parse
    @data
  end
end
